#[derive(Debug)]
pub struct Node<'a> {
    pub tag_name: &'a str,
    pub text_contents: Option<&'a str>,
    pub children: Vec<Node<'a>>,
}

impl Node<'_> {
    pub fn new(tag_name: &str) -> Node {
        Node {
            tag_name: tag_name,
            text_contents: None,
            children: Vec::new(),
        }
    }
}
